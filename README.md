# Vue flying text

## Table of Contents

- [About](#about)
- [Getting Started](#getting_started)

## About <a name = "about"></a>

Небольшое демонстративое веб приложение, написанное на vuejs с использованием библиотек Velocity (анимация) и Spectre (стили).

## Getting Started <a name = "getting_started"></a>

Для ознакомления достатоно скачать index1.html и открыть в любом современном браузере.

